from django.contrib import admin
from .models import Character, CharacterClass, GameServer, Item, ItemType, Contact, Faq


class CharacterAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "name",
        "server",
        "hero_class",
        "level",
        "description",
        "added",
        "price",
    )


class CharacterClassAdmin(admin.ModelAdmin):
    list_display = ("id", "initials", "name")


class GameServerAdmin(admin.ModelAdmin):
    list_display = ("id", "name", "adenaRatio")


class ItemTypeAdmin(admin.ModelAdmin):
    list_display = ("id", "name")


class ItemAdmin(admin.ModelAdmin):
    list_display = ("id", "name", "type", "server", "status")


class ContactAdmin(admin.ModelAdmin):
    list_display = ("id", "name", "skype", "discord", "telegram", "mail", "other")


class FaqAdmin(admin.ModelAdmin):
    list_display = ("id", "position", "question")

# Register your models here.
admin.site.register(Character, CharacterAdmin)
admin.site.register(CharacterClass, CharacterClassAdmin)
admin.site.register(GameServer, GameServerAdmin)
admin.site.register(Item, ItemAdmin)
admin.site.register(ItemType, ItemTypeAdmin)
admin.site.register(Contact, ContactAdmin)
admin.site.register(Faq, FaqAdmin)
