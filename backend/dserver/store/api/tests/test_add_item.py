from json import dumps, loads
import datetime

from graphene.test import Client
import pytest

from store.schema import schema

from store.models import GameServer, CharacterClass, Character, ItemType, Item, Contact, Faq

pytestmark = pytest.mark.django_db

def to_dict(input_ordered_dict):
    return loads(dumps(input_ordered_dict))


def expect_error(response, message):
    result = to_dict(response["errors"])
    assert result[0]["message"] == message


ADD_ITEM_MUTATION = """
    mutation addItem($inputData: AddItemInput!) {
        addItem(inputData: $inputData) {
            addedItem {
                id
            }
        }
    }
"""


def request_add_item(item_type, contact_id, server_id):
    client = Client(schema)
    return client.execute(
        ADD_ITEM_MUTATION,
        variables={
            "inputData": {
                "name": "ex1",
                "itemType": item_type,
                "server": server_id,
                "price": 30,
                "contact": contact_id
            }
        },
    )


def create_server():
    server = GameServer(name='a', adenaRatio=0.20)
    server.save()
    return server


def create_contact():
    contact = Contact(name='b', skype="kune@gunda.store")
    contact.save()
    return contact


def create_item_type():
    item_type = ItemType(name='typeA')
    item_type.save()
    return item_type


def test_add_item__success():
    server = create_server()
    contact = create_contact()
    item_type = create_item_type()

    new_item_id = 1

    assert not Item.objects.filter(id=new_item_id).exists()

    response = request_add_item(item_type.id, contact.id, server.id)
    addedItem = to_dict(response["data"]["addItem"])["addedItem"]
    assert addedItem["id"] == str(new_item_id)

    assert Item.objects.filter(id=new_item_id).exists()
