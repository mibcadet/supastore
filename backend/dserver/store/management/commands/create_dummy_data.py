from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth import get_user_model
from store.models import GameServer, CharacterClass, Character, ItemType, Item, Contact, Faq


class Command(BaseCommand):
    help = "Creates dummy data"

    def handle(self, *args, **options):
        self.flush()
        self.createAdmin()

        self.createServers(["EU RED", "EU GIRAN", "US WAT"])
        self.createClasses([("Ghost Sentinel", "GS"), ("Storm Screamer", "SS")])
        self.createItemTypes(["Weapon", "Armor", "Jewel", "Others"])
        self.createContact(["Kunegunda", "Oguur", "Pomocnik obiboka"])
        self.createFaqs(["What can I buy?", "How can I pay?", "Am I secure?"])

        gs = CharacterClass.objects.get(name="Ghost Sentinel")
        euRed = GameServer.objects.get(name="EU RED")
        usWat = GameServer.objects.get(name="US WAT")
        weapon = ItemType.objects.get(name="Weapon")
        armor = ItemType.objects.get(name="Armor")
        contact = Contact.objects.get(name="Kunegunda")

        self.createItems(["Tarbar", "Draconic Bow", "Arcana Mace"], euRed, weapon, contact, 'W')
        self.createItems(["Tarbar", "Draconic Bow", "Arcana Mace"], euRed, weapon, contact, 'A')
        self.createItems(["Nurz", "Hook of Captain Hook", "Anime strings"], euRed, armor, contact, 'A')
        self.createCharacter(gs, euRed, 33, "Lorem ipsum #1", 9, contact, 'A')
        self.createCharacter(gs, euRed, 1, "Lorem ipsum #2", 13, contact, 'A')
        self.createCharacter(gs, usWat, 122, "Lorem ipsum #3", 64, contact, 'A')

        self.stdout.write(
            self.style.SUCCESS(
                'Successfully created dummy data, also created super user with username "admin" and password "admin"'
            )
        )

    def flush(self):
        User = get_user_model()

        for model in [User, Character, Item, CharacterClass, GameServer, ItemType, Contact, Faq]:
            model.objects.all().delete()

    def createAdmin(self):
        User = get_user_model()
        u = User(username="admin", email="admin@admin.com")
        u.set_password("admin")
        u.is_superuser = True
        u.is_staff = True
        u.save()

    def createCharacter(self, characterClass, server, level, desc, price, contact, status):
        character = Character(
            hero_class=characterClass,
            server=server,
            level=level,
            description=desc,
            price=price,
            contact=contact,
            status=status,
        )
        character.save()

    def createServers(self, names):
        for name in names:
            server = GameServer(name=name, adenaRatio=0.20)
            server.save()

    def createContact(self, names):
        for name in names:
            contact = Contact(name=name, skype="kune@gunda.store")
            contact.save()

    def createClasses(self, classes):
        for name, initials in classes:
            characterClass = CharacterClass(name=name, initials=initials)
            characterClass.save()

    def createItemTypes(self, names):
        for name in names:
            itemType = ItemType(name=name)
            itemType.save()

    def createItems(self, names, server, type, contact, status):
        for name in names:
            item = Item(
                name=name,
                server=server,
                type=type,
                price=100,
                contact=contact,
                status=status
            )
            item.save()

    def createFaqs(self, questions):
        pos = 1
        for question in questions:
            pos = pos + 1
            faq = Faq(
                position=pos,
                question=question,
                answer="Lorem ipsum et dolor takie tam bo nie znam żadnych odpowiedzi \
                        więc piszę coś żeby było widać jakiś tekst który przeczyta dobry klient.")
            faq.save()
