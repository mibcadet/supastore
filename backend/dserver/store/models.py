from django.db import models

STATUSES = (
    ('A', 'Approved'),
    ('W', 'Waiting'),
    ('D', 'Declined'),
)

class GameServer(models.Model):
    name = models.CharField(max_length=60)
    adenaRatio = models.FloatField(default=0.20)

    def __str__(self):
        return self.name


class Contact(models.Model):
    name = models.CharField(max_length=200)
    skype = models.TextField(blank=True, null=True)
    discord = models.TextField(blank=True, null=True)
    telegram = models.TextField(blank=True, null=True)
    mail = models.TextField(blank=True, null=True)
    other = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.name


class CharacterClass(models.Model):
    initials = models.CharField(max_length=3)
    name = models.CharField(max_length=120)

    def __str__(self):
        return self.name


class Character(models.Model):
    name = models.CharField(max_length=120)
    hero_class = models.ForeignKey(CharacterClass, on_delete=models.CASCADE)
    server = models.ForeignKey(GameServer, on_delete=models.CASCADE)
    level = models.IntegerField()
    description = models.TextField(blank=True, null=True, default='')
    added = models.DateField(auto_now_add=True)
    price = models.FloatField()
    contact = models.ForeignKey(Contact, on_delete=models.CASCADE, null=True, blank=True)
    request_contact = models.TextField(blank=True, default='')
    status = models.CharField(max_length=20, choices=STATUSES, default='W')

    def __str__(self):
        return self.name


class ItemType(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name

class Item(models.Model):
    name = models.CharField(max_length=200)
    type = models.ForeignKey(ItemType, on_delete=models.CASCADE)
    server = models.ForeignKey(GameServer, on_delete=models.CASCADE)
    price = models.FloatField()
    contact = models.ForeignKey(Contact, on_delete=models.CASCADE, null=True, blank=True)
    request_contact = models.TextField(blank=True, default='')
    status = models.CharField(max_length=20, choices=STATUSES, default='W')

    def __str__(self):
        return self.name


class Faq(models.Model):
    position = models.IntegerField()
    question = models.TextField()
    answer = models.TextField()

    def __str__(self):
        return self.question
