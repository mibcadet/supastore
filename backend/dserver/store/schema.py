import graphene

from graphene_django.types import DjangoObjectType

from .models import (
    Character as CharacterModel,
    CharacterClass as CharacterClassModel,
    GameServer as GameServerModel,
    Item as ItemModel,
    ItemType as ItemTypeModel,
    Contact as ContactModel,
    Faq as FaqModel,
)


class Server(DjangoObjectType):
    class Meta:
        model = GameServerModel


class CharacterClass(DjangoObjectType):
    class Meta:
        model = CharacterClassModel


class Character(DjangoObjectType):
    class Meta:
        model = CharacterModel


class ItemType(DjangoObjectType):
    class Meta:
        model = ItemTypeModel


class Item(DjangoObjectType):
    class Meta:
        model = ItemModel

class Contact(DjangoObjectType):
    class Meta:
        model = ContactModel

class Faq(DjangoObjectType):
    class Meta:
        model = FaqModel


class Query(graphene.ObjectType):
    all_characters = graphene.List(Character)
    all_character_classes = graphene.List(CharacterClass)
    all_servers = graphene.List(Server)
    all_items = graphene.List(Item)
    all_item_types = graphene.List(ItemType)
    all_faqs = graphene.List(Faq)

    def resolve_all_characters(self, info, **kwargs):
        return CharacterModel.objects.all()

    def resolve_all_character_classes(self, info, **kwargs):
        return CharacterClassModel.objects.all()

    def resolve_all_servers(self, info, **kwargs):
        return GameServerModel.objects.all()

    def resolve_all_items(self, info, **kwargs):
        return ItemModel.objects.filter(status='A')

    def resolve_all_item_types(self, info, **kwargs):
        return ItemTypeModel.objects.all()

    def resolve_all_faqs(self, info, **kwargs):
        return FaqModel.objects.all()


class AddCharacterInput(graphene.InputObjectType):
    name = graphene.String(required=True)
    hero_class = graphene.String(required=True)
    server = graphene.String(required=True)
    level = graphene.Int(required=True)
    description = graphene.String()
    price = graphene.Float(required=True)
    request_contact = graphene.String(required=True)


class AddCharacter(graphene.Mutation):

    added_character = graphene.Field(Character)

    class Arguments:
        input_data = AddCharacterInput(required=True)

    def mutate(self, info, input_data):

        character = CharacterModel(name = input_data.name,
                                   hero_class_id = input_data.hero_class,
                                   server_id = input_data.server,
                                   level = input_data.level,
                                   description = input_data.description,
                                   price = input_data.price,
                                   contact = None,
                                   request_contact = input_data.request_contact)
        character.save()

        return AddCharacter(character)

class AddItemInput(graphene.InputObjectType):
    name = graphene.String(required=True)
    item_type = graphene.String(required=True)
    server = graphene.String(required=True)
    price = graphene.Float(required=True)
    request_contact = graphene.String(required=True)

class AddItem(graphene.Mutation):

    added_item = graphene.Field(Item)

    class Arguments:
        input_data = AddItemInput(required=True)

    def mutate(self, info, input_data):

        item_type = ItemTypeModel.objects.get(name=input_data.item_type)
        server = GameServerModel.objects.get(name=input_data.server)
        item = ItemModel(name=input_data.name,
                         type_id=item_type.id,
                         server_id=server.id,
                         price=input_data.price,
                         contact=None,
                         request_contact=input_data.request_contact)
        item.save()

        return AddItem(item)


class Queries(Query, graphene.ObjectType):
    pass


class Mutations(graphene.ObjectType):
    add_item = AddItem.Field()
    add_character = AddCharacter.Field()


schema = graphene.Schema(query=Queries, mutation=Mutations)
