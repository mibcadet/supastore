#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'

APP_DIR=$(dirname "$(dirname "$0")")
cd "${APP_DIR}"

python dserver/manage.py create_dummy_data