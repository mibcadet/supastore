#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'

APP_DIR=$(dirname "$(dirname "$0")")
cd "${APP_DIR}"

python3 dserver/manage.py runserver 0.0.0.0:8000
