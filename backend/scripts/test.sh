#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'

APP_DIR=$(dirname "$(dirname "$0")")
cd "${APP_DIR}"

echo 'Running backend tests..'
cd dserver && pytest . --disable-pytest-warnings
