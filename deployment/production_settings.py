from .settings import *

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

#SESSION_COOKIE_SECURE = True
SECURE_SSL_REDIRECT = False
CSRF_COOKIE_SECURE = True
DEBUG = False

SECRET_KEY = 'gdfsa%%9kcyk4t2!db!97nvz-_yjt339rv$*y$($a8u9o1+krcfbo+$232'
ALLOWED_HOSTS = ['*']

STATIC_ROOT = "/opt/shop/frontend/static"
