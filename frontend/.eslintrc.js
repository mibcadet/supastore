module.exports = {
    env: {
        jest: true,
        es6: true,
        node: true
    },
    extends: ['prettier'],
    plugins: ['prettier'],
    parser: 'babel-eslint',
    parserOptions: {
        ecmaVersion: 2017,
        ecmaFeatures: {
            experimentalObjectRestSpread: true
        },
        sourceType: 'module'
    },
    rules: {
        'object-curly-spacing': ['error', 'always'],
        camelcase: 0,
        'sort-imports': [
            'error',
            {
                ignoreCase: false,
                ignoreDeclarationSort: true,
                ignoreMemberSort: false,
                memberSyntaxSortOrder: ['none', 'all', 'multiple', 'single']
            }
        ],
        'prettier/prettier': 'error'
    }
};
