set -euo pipefail
IFS=$'\n\t'

APP_DIR=$(dirname "$(dirname "$0")")
cd "${APP_DIR}"

echo "Formatting..."
yarn format