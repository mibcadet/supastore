import * as React from "react";
import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

import { FacebookOutlined, SkypeFilled } from '@ant-design/icons';

import { Affix, Button, Layout, Menu, Typography } from "antd";

import ApolloClient from "apollo-boost";
import { ApolloProvider } from "@apollo/react-hooks";

import { Shop } from 'components/shop';
import { Faqs } from 'components/faq';
import { Middleman } from 'components/middleman';

const { Header } = Layout;

const { Title } = Typography;

const client = new ApolloClient({
  uri: "/graphql"
});

const App: React.FC = () => {

  return (
    <ApolloProvider client={client}>
      <Router>
        <Layout style={{height: "100%"}}>
          <Header style={{padding: 0, display: 'flex', justifyContent: 'space-between', height: 64, width: "100%"}}>
            <div style={{width: '200px', display: 'inline-flex', justifyContent: "center"}}>
              <Title level={4} style={{lineHeight: "64px", color: "#ffffff"}}>STANX STORE</Title>
            </div>
            <Menu
              theme="dark"
              mode="horizontal"
              style={{ alignSelf: 'flex-end'}}>
              <Menu.Item key="/"><Link to="/">Shop</Link></Menu.Item>
              <Menu.Item key="/middleman"><Link to="/middleman">Middleman service</Link></Menu.Item>
              <Menu.Item key="/faq"><Link to="/faq">F.A.Q.</Link></Menu.Item>
            </Menu>
            <img src="/logo.png" style={{height: "64px"}} alt="logo" />
          </Header>
          <Layout>
            <Switch>
              <Route exact path="/">
                <Shop />
              </Route>
              <Route path="/middleman">
                <Middleman />
              </Route>
              <Route path="/faq">
                <Faqs />
              </Route>
            </Switch>
            <Affix style={{ position: "fixed", bottom: 30, right: 50 }}>
              <a href="https://join.skype.com/invite/nhf4cXuS2LPv" target="_blank" rel="noopener noreferrer">
                <Button type="primary" shape="circle" size="large">
                  <SkypeFilled style={{ fontSize: "24px" }} />
                </Button>
              </a>
            </Affix>
            <Affix style={{ position: "fixed", bottom: 80, right: 50 }}>
              <a href="https://www.facebook.com/stanxstore/" target="_blank" rel="noopener noreferrer">
                <Button style={{backgroundColor: "#4267b2"}} shape="circle" size="large">
                  <FacebookOutlined style={{ fontSize: "24px", color: "#fff" }} />
                </Button>
              </a>
            </Affix>
          </Layout>
        </Layout>
      </Router>
      <div style={{
        position: 'fixed',
        height: '100%',
        width: '100%',
        top: 80,
        background: 'url("/bg.png")',
        backgroundRepeat: "no-repeat",
        backgroundSize: 'cover',
        pointerEvents: 'none'
      }} />
    </ApolloProvider>
  );
};

export { App };
