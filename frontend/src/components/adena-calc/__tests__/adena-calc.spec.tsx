import * as React from 'react';
import { mount } from 'enzyme';
import { act } from 'react-dom/test-utils';

import { Input } from 'antd';
import { AdenaCalc } from '../adena-calc.component'

describe('Adena calc', () => {
  it('should render', () => {
    const component = mount(<AdenaCalc ratio={0.45} />)

    expect(component).toBeTruthy();
  });

  it('should handle input change to display proper output', () => {
    const component = mount(<AdenaCalc ratio={0.45} />)

    act(() => {
      component.find('input[value="$ 3.00"]').simulate('change', { target: { value: "$ 2.00"}});
    });
    component.update();

    expect(component.find(Input).first().props().value).toEqual("4444444");
  });
});
