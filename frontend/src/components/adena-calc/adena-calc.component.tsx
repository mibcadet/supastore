import * as React from 'react';
import { useEffect, useState } from 'react';
import { DollarOutlined, SwapOutlined, SkypeFilled } from '@ant-design/icons';
import { InputNumber, Typography, Input, Popover, Button } from 'antd';

const {Text} = Typography;

interface IProps {
  ratio: number;
}

const minMoney = 1;
const maxMoney = 100000;

const AdenaCalc: React.FC<IProps> = ({ratio}) => {

  const [userMoney, setUserMoney] = useState<number>(3);
  const [offer, setOffer] = useState<number>(0);

  const onChange = (value: number | undefined) => {
    if (value && value >= minMoney && value <= maxMoney)
      value && setUserMoney(value);
  }

  const restoreCalculatedValue = () => {
      setUserMoney(userMoney);
  }

  useEffect(() => {
    const sum = ratio > 0 ? (userMoney / ratio) * 1000000 : 0;
    setOffer(sum);
  }, [userMoney, ratio])

  return <>
    {
      <div style={{
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'start'
      }} >
        <div style={{
          display: 'flex',
          flexDirection: 'column',
          justifyContent: 'center',
          alignItems: 'center',
          width: 90
        }}>
          <DollarOutlined style={{fontSize: 36}} />
          <Text>{ratio}$/m</Text>
        </div>
        <InputNumber disabled={ratio <= 0} style={{width: 150}} precision={2} min={minMoney} max={maxMoney} defaultValue={3} onChange={onChange} formatter={value => `$ ${value}` } />
        <SwapOutlined />
        <Popover placement="bottomLeft" content={<Text>Select server to check rates</Text>} visible={ ratio <= 0}>
          <Input addonAfter="Adena" value={offer.toFixed(0)} onChange={restoreCalculatedValue} />
        </Popover>
        <a href="https://join.skype.com/invite/nhf4cXuS2LPv" target="_blank" rel="noopener noreferrer">
          <Button type="primary" icon={<SkypeFilled style={{fontSize: 18}} />} style={{fontSize: 15}}> Buy </Button>
        </a>
      </div>
    }
  </>;
}

export { AdenaCalc };
