import * as React from 'react';
import { List } from 'antd';
import { Character } from 'lib/graphql';


interface IProps {
  detailsData: Character;
}

const CharacterDetails: React.FC<IProps> = ({detailsData}) => {

  return (
    <List>
      <List.Item>Server: {detailsData.server}</List.Item>
      <List.Item>Class: {detailsData.heroClass}</List.Item>
      <List.Item>Level: {detailsData.level}</List.Item>
      <List.Item>Description: <pre>{detailsData.description}</pre></List.Item>
      <List.Item>Price: {detailsData.price}</List.Item>
    </List>
  );
};

export { CharacterDetails };
