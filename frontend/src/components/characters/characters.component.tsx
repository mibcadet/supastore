import * as React from "react";
import { useEffect, useState } from "react";

import { Modal, Spin, Table } from "antd";

import {
  useCharactersList,
  useCharacterClassesList,
  ICharacterClass,
  useServersList,
  IServer,
  ICharacter
} from "services/characters-to-table/characters-to-table";
import { Contact as ContactType, Character } from "lib/graphql";
import { Contact } from "components/contact";
import { CharacterDetails } from 'components/characters';

interface ICharactersData {
  characters: ICharacter[];
  characterClasses: ICharacterClass[];
  servers: IServer[];
}

const useCharacterTableData = (): [ICharactersData, boolean] => {
  const [characters, charactesLoading] = useCharactersList();
  const [characterClasses, classesLoading] = useCharacterClassesList();
  const [servers, serversLoading] = useServersList();

  const [data, setData] = useState<ICharactersData>({
    characters: [],
    characterClasses: [],
    servers: []
  });
  const [loading, setLoading] = useState<boolean>(false);

  useEffect(() => {
    const fullData = {
      characters,
      characterClasses,
      servers
    };

    if (!loading && fullData) {
      setData(fullData);
    }
  }, [characters, characterClasses, servers, loading]);

  useEffect(() => {
    setLoading(charactesLoading || classesLoading || serversLoading);
  }, [charactesLoading, classesLoading, serversLoading]);

  return [data, loading];
};

interface IFilter {
  text: string;
  value: string;
}

interface IProps {
  serverName: string;
}

const Characters: React.FC<IProps> = ({ serverName }) => {
  const [columns, setColumns] = useState<any[]>([]);
  const [fullData, loading] = useCharacterTableData();
  const [filteredData, setFilteredData] = useState<any[]>([]);
  const [contactIsVisible, setContactIsVisible] = useState<boolean>(false);
  const [contactData, setContactData] = useState<ContactType>();
  const [detailsData, setDetailsData] = useState<Character>();

  useEffect(() => {
    const classFilters: IFilter[] = fullData.characterClasses.map(
      (element: ICharacterClass) => {
        return { text: element.name, value: element.name };
      }
    );

    setColumns(getColumnsSchema(classFilters));
  }, [fullData]);

  useEffect(() => {
    if (serverName) {
      const filtered = fullData.characters.filter(character => {
        return character.gameServer === serverName;
      });

      setFilteredData(filtered);
    } else {
      setFilteredData([]);
    }
  }, [fullData, serverName]);

  return (
    <>
      {loading && <Spin size="large" />}
      {!loading && (
        <Table
          size="small"
          bordered={true}
          columns={columns}
          dataSource={serverName ? filteredData : fullData.characters}
          onRow={(record: Character) => {
            return {
              onClick: () => {
                setDetailsData(record);
                if (record.contact) setContactData(record.contact);
                setContactIsVisible(true);
              }
            };
          }}
        />
      )}
      <Modal
        title="Details and contact"
        footer=""
        onCancel={() => {
          setContactIsVisible(false);
        }}
        visible={contactIsVisible}
      >
        {detailsData && <CharacterDetails detailsData={detailsData} />}
        <Contact contactData={contactData} />
      </Modal>
    </>
  );
};

export { Characters };

function getColumnsSchema(classFilters: IFilter[]) {
  return [
    {
      title: "Class",
      dataIndex: "heroClass",
      key: "heroClass",
      filters: classFilters,
      ellipsis: true,
      width: 250,
      render: (text: any) => <strong>{text}</strong>,
      onFilter: (value: string, record: ICharacter) => {
        return record.heroClass.indexOf(value) === 0;
      },
      sorter: (a: ICharacter, b: ICharacter) =>
        ("" + a.name).localeCompare(b.name)
    },
    {
      title: "Lvl",
      dataIndex: "level",
      key: "level",
      width: 60,
      sorter: (a: ICharacter, b: ICharacter) => a.level - b.level
    },
    {
      title: "Server",
      dataIndex: "gameServer",
      key: "gameServer",
      ellipsis: true,
      width: 140,
      sorter: (a: ICharacter, b: ICharacter) =>
        ("" + a.gameServer).localeCompare(b.gameServer)
    },
    {
      title: "Description",
      dataIndex: "description",
      key: "description",
      ellipsis: true
    },
    {
      title: "Price",
      dataIndex: "price",
      key: "price",
      width: 70,
      sorter: (a: ICharacter, b: ICharacter) => a.price - b.price,
      render: (text: string) => `$${text}`
    }
  ];
}
