import { Characters } from "./characters.component";
import { CharacterDetails } from "./character-details.component";

export { Characters, CharacterDetails };
