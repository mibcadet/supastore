import * as React from 'react';
import { mount } from 'enzyme';

import { List } from 'antd';
import { Contact } from '../contact.component'
import { Contact as ContactType } from 'lib/graphql';

describe('Contact component', () => {
  it('should render', () => {
    const component = mount(<Contact />)

    expect(component).toBeTruthy();
  });

  it('should render discord position when discrord contact is given', () => {
    const contactData: ContactType = {
      id: '123',
      name: 'contact name',
      discord: 'discord@contact',
      characterSet: [],
      itemSet: []
    };
    const component = mount(<Contact contactData={contactData} />)

    expect(component.find(List.Item).find('a').text()).toEqual('Discord');
  });

  it('should render other position when discrord contact is given', () => {
    const contactData: ContactType = {
      id: '123',
      name: 'contact name',
      other: 'other@contact',
      characterSet: [],
      itemSet: []
    };
    const component = mount(<Contact contactData={contactData} />)

    expect(component.find(List.Item).find('a').text()).toEqual('other@contact');
  });
});
