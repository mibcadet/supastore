import * as React from "react";

import { List, Button, Typography} from "antd";
import { Contact as ContactType } from "lib/graphql";
import { SkypeFilled } from "@ant-design/icons";

interface IProps {
  contactData?: ContactType;
}

const { Text } = Typography;

const Contact: React.FC<IProps> = ({ contactData }) => {
  return (
    <>
      <Text>Select method of contact:</Text>
      {contactData && (
        <List>
          {contactData.skype && (
            <List.Item>
              <a href={contactData.skype} target="_blank" rel="noopener noreferrer">
                <Button type="primary" icon={<SkypeFilled />}>
                  Skype
                </Button>
              </a>
            </List.Item>
          )}
          {contactData.discord && (
            <List.Item>
              <a href={contactData.discord} target="_blank" rel="noopener noreferrer">Discord</a>
            </List.Item>
          )}
          {contactData.telegram && (
            <List.Item>
              <a href={contactData.telegram} target="_blank" rel="noopener noreferrer">Telegram</a>
            </List.Item>
          )}
          {contactData.mail && (
            <List.Item>
              <a href={contactData.mail} target="_blank" rel="noopener noreferrer">Mail</a>
            </List.Item>
          )}
          {contactData.other && (
            <List.Item>
              <a href={contactData.other} target="_blank" rel="noopener noreferrer">
                <Button type="primary">
                  {contactData.other}
                </Button>
              </a>
            </List.Item>
          )}
        </List>
      )}
    </>
  );
};

export { Contact };
