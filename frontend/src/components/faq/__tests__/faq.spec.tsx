import * as React from 'react';
import { mount } from 'enzyme';
import { waitForComponentToPaint } from 'services/waitForComopnentToUpdate';

import { ApolloProvider } from '@apollo/react-hooks';
import { createMockClient } from 'mock-apollo-client';

import { FaqsDocument } from 'lib/graphql'

import { Faqs } from '../faq.component';

const faqsQueryDoc = jest.fn().mockResolvedValue({
  data: {
    allFaqs: {
      id: '123',
      position: '2',
      question: 'is it?',
      answer: 'yes it is'
    },
  }
});

const mockClient = createMockClient();
mockClient.setRequestHandler(FaqsDocument, faqsQueryDoc);

describe('FAQ', () => {

  it('should render FAQ', async () => {

    const component = mount(<ApolloProvider client={mockClient}><Faqs /></ApolloProvider>);
    await waitForComponentToPaint(component);

    expect(component.text()).toEqual("Frequently Asked Questions");
  });
});
