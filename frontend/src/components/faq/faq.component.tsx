import * as React from 'react';
import { useEffect, useState } from 'react';

import { useFaqsQuery, Faq } from 'lib/graphql';

import { Collapse } from 'antd';
import { Col, Layout, PageHeader, Row } from 'antd';

const { Content, Sider } = Layout;

const { Panel } = Collapse;

const Faqs: React.FC = () => {

  const { data, loading, error } = useFaqsQuery();
  const [ faqs, setFaqs ] = useState<Faq[]>([]);
  const [isCollapsed, setIsCollapsed] = useState<boolean>(false);

  useEffect(() => {
    if (!loading && !error && data)
      setFaqs(data.allFaqs as Faq[]);
  }, [data, loading, error]);

  return (
    <Layout>
      <Sider
        collapsible
        collapsed={isCollapsed}
        onCollapse={(collapsed) => { setIsCollapsed(collapsed); } } />
      <Layout>
        <Content>
          <PageHeader title="Frequently Asked Questions" />
          <Row>
            <Col span={22} offset={1}>
              <Collapse>
                {
                  faqs && faqs.length > 0 && faqs.map(faq => {
                    return (
                      <Panel header={faq.question} key={faq.id}>
                        <pre>
                          {faq.answer}
                        </pre>
                      </Panel>
                    )
                  })
                }
              </Collapse>
            </Col>
          </Row>
        </Content>
      </Layout>
    </Layout>
  );
};

export { Faqs };
