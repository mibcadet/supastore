import { ItemDetails } from "./item-details.component";
import { Items } from "./items.component";

export { Items, ItemDetails };
