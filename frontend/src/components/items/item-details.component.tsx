import * as React from 'react';
import { List } from 'antd';
import { Item } from "lib/graphql";

interface IProps {
  detailsData: Item;
}

const ItemDetails: React.FC<IProps> = ({detailsData}) => {

  return (
    <List>
      <List.Item>Server: {detailsData.server}</List.Item>
      <List.Item>Item: {detailsData.name}</List.Item>
      <List.Item>Price: {detailsData.price}</List.Item>
    </List>
  );
};

export { ItemDetails };
