import * as React from "react";
import { useEffect, useState } from "react";

import { Modal, Spin, Table } from "antd";
import {
  useItemsList,
  IItem,
  IItemType,
  useItemTypes
} from "services/items-to-table/items-to-table";

import { Contact } from "components/contact";
import { Contact as ContactType, Item } from "lib/graphql";
import { ItemDetails } from 'components/items';
import { ShoppingCartOutlined } from "@ant-design/icons";

interface IItemsData {
  items: IItem[];
  itemTypes: IItemType[];
}

const useItemsTableData = (): [IItemsData, any] => {
  const [items, itemsLoading] = useItemsList();
  const [itemTypes, itemTypesLoading] = useItemTypes();

  const [data, setData] = useState<IItemsData>({ items: [], itemTypes: [] });
  const [loading, setLoading] = useState<boolean>(false);

  useEffect(() => {
    const fullData = {
      items,
      itemTypes
    };

    if (!loading && fullData) {
      setData(fullData);
    }
  }, [items, itemTypes, loading]);

  useEffect(() => {
    setLoading(itemsLoading || itemTypesLoading);
  }, [itemsLoading, itemTypesLoading]);

  return [data, loading];
};

interface IFilter {
  text: string;
  value: string;
}

interface IProps {
  serverName: string;
}

const Items: React.FC<IProps> = ({ serverName }) => {
  const [columns, setColumns] = useState<any[]>([]);
  const [fullData, loading] = useItemsTableData();
  const [filteredData, setFilteredData] = useState<any[]>([]);
  const [contactIsVisible, setContactIsVisible] = useState<boolean>(false);
  const [contactData, setContactData] = useState<ContactType>();
  const [detailsData, setDetailsData] = useState<Item>();

  useEffect(() => {
    const typeFilters: IFilter[] = fullData.itemTypes.map(
      (element: IItemType) => {
        return { text: element.name, value: element.name };
      }
    );

    setColumns(getColumnsSchema(typeFilters));
  }, [fullData]);

  useEffect(() => {
    if (serverName) {
      const filtered = fullData.items.filter(
        item => item.server === serverName
      );
      setFilteredData(filtered);
    } else {
      setFilteredData([]);
    }
  }, [fullData, serverName]);

  return (
    <>
      {loading && <Spin size="large" />}
      {!loading && (
        <Table
          columns={columns}
          size="small"
          bordered={true}
          onRow={(record) => {
            return {
              onClick: () => {
                setDetailsData(record);
                setContactData(record.contact);
                setContactIsVisible(true);
              }
            };
          }}
          dataSource={serverName ? filteredData : fullData.items}
        />
      )}
      <Modal
        title="Item details and contact"
        footer=""
        onCancel={() => setContactIsVisible(false)}
        visible={contactIsVisible}
      >
          {detailsData && <ItemDetails detailsData={detailsData} />}
          <Contact contactData={contactData} />
      </Modal>
    </>
  );

  function getColumnsSchema(typeFilters: IFilter[]) {
    return [
      {
        title: "Name",
        dataIndex: "name",
        key: "name",
        ellipsis: true,
        render: (text: any) => <strong>{text}</strong>,
        sorter: (a: IItem, b: IItem) => ("" + a.name).localeCompare(b.name)
      },
      {
        title: "Server",
        dataIndex: "server",
        key: "server",
        width: 140,
        ellipsis: true,
      },
      {
        title: "Type",
        dataIndex: "type",
        key: "type",
        ellipsis: true,
        filters: typeFilters,
        width: 70,
        onFilter: (value: string, record: IItem) =>
          record.type.indexOf(value) === 0
      },
      {
        title: "Price",
        dataIndex: "price",
        key: "price",
        width: 70,
        render: (text: string) => `$${text}`,
        sorter: (a: IItem, b: IItem) => a.price - b.price
      },
      {
        title: "Buy",
        key: "id",
        width: 50,
        render: () =>
          <a href="https://join.skype.com/invite/nhf4cXuS2LPv" target="_blank" rel="noopener noreferrer">
            <ShoppingCartOutlined style={{fontSize: 24, color: '@primaryColor'}} />
          </a>
      }
    ];
  }
};

export { Items };
