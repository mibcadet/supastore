import * as React from 'react';
import { useState } from 'react';

import {
  CheckCircleOutlined,
  DollarOutlined,
  GiftOutlined,
  MessageOutlined,
  SmileOutlined,
} from '@ant-design/icons';

import { Button, Card, Col, Layout, PageHeader, Row, Timeline, Typography } from 'antd';

const { Paragraph, Text, Title } = Typography;
const { Content, Sider } = Layout;

const Middleman: React.FC = () => {

  const [isCollapsed, setIsCollapsed] = useState<boolean>(false);

  return (
    <Layout>
      <Sider
        collapsible
        collapsed={isCollapsed}
        onCollapse={(collapsed) => { setIsCollapsed(collapsed); } } />
      <Content>
        <PageHeader title="Middleman service - The safest way to sell/buy your virtual goods" />
        <Layout>
          <Row>
            <Col md={{span: 22, offset: 1 }} lg={{span: 11, offset: 1}}>
              <Card
                title={<CardTitle text="Buyer" />}
                style={{height: "100%"}}>
                <Paragraph>You will always receive item you paid for.</Paragraph>
                <Paragraph>With our help GMs/Administration will never track your trade what prevent you from getting ban.</Paragraph>
              </Card>
            </Col>
            <Col md={{span: 22, offset: 1 }} lg={{span: 11, offset: 0}}>
              <Card
                title={<CardTitle text="Seller" />}
                style={{height: "100%"}}>
                <Paragraph>Safe and fast deal.</Paragraph>
                <Paragraph>Protect seller from money recall.</Paragraph>
              </Card>
            </Col>
          </Row>
          <br />
          <Row>
            <Col md={{span: 22, offset: 1}}>
              <Card title={
                <CardTitle text="How does the middleman service look?" position="center" />
              }>
                <Row justify="center">
                  <Timeline>
                    <Timeline.Item
                      dot={<MessageOutlined style={{ fontSize: '20px' }} />}
                      color="blue">We create special chat where seller and buyer must join.</Timeline.Item>
                    <Timeline.Item
                      dot={<GiftOutlined style={{ fontSize: '20px' }} />}
                      color="blue">Seller provides items to the Middleman, we describe to buyer what we got.</Timeline.Item>
                    <Timeline.Item
                      dot={<DollarOutlined style={{ fontSize: '20px' }} />}
                      color="blue">Buyer sends money to our wallet.</Timeline.Item>
                    <Timeline.Item
                      dot={<CheckCircleOutlined style={{ fontSize: '20px' }} />}
                      color="green">We check the payment. If everything is ok, the buyer receives the items.</Timeline.Item>
                    <Timeline.Item
                      dot={<SmileOutlined style={{ fontSize: '20px' }} />}
                      color="green">Seller receives money for his goods.</Timeline.Item>
                  </Timeline>
                </Row>
                <Row justify="center">
                  <Title level={4}><Text style={{color: "#DD0000"}}>SALE!</Text>: Standard Middleman service now at ultra low price!</Title>
                </Row>
                <Row justify="center">
                  <Title level={4}>Commission - 5% from deal or 5$ if less than 100$!</Title>
                </Row>
                <Row justify="center">
                  <a href="https://join.skype.com/invite/nhf4cXuS2LPv" target="_blank" rel="noopener noreferrer">
                    <Button type="primary" size="large">Use Middleman</Button>
                  </a>
                </Row>
              </Card>
            </Col>
          </Row>
        </Layout>
      </Content>
    </Layout>
  );
};

const CardTitle: React.FC<{text: string, position?: 'start'|'end'|'center'}> = ({text, position}) => {
  return (
    <Row justify={position || 'start'}>
      <Title level={4}>{text}</Title>
    </Row>
  );
}

export { Middleman };
