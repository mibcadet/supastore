import * as React from "react";
import { useCallback, useState } from "react";

import { useServersList } from "services/characters-to-table/characters-to-table";

import '@ant-design/compatible/assets/index.css';

import {
  Button,
  Card,
  Col,
  Form,
  Input,
  InputNumber,
  Layout,
  Modal,
  PageHeader,
  Result,
  Row,
  Select,
} from "antd";

import { Items } from "components/items";
import { Characters } from "components/characters";
import { SideMenu } from "components/side-menu";
import { AdenaCalc } from "components/adena-calc/adena-calc.component";
import { useItemTypes } from "services/items-to-table/items-to-table";
import { useAddItemMutation, AddItemInput } from "lib/graphql";

const { Sider, Content } = Layout;
const { Option } = Select;

const Shop: React.FC = () => {

  const [selectedServer, setSelectedServer] = useState<string>("");
  const [adenaRatio, setAdenaRatio] = useState<number>(0.2);
  const [isCollapsed, setIsCollapsed] = useState<boolean>(false);
  const [isAddItemVisible, setAddItemVisible] = useState<boolean>(false);
  const [isAddCharacterVisible, setCharacterVisible] = useState<boolean>(false);
  const [[isResultVisible, isSuccess], setResultVisible] = useState<[boolean, boolean]>([false, false]);
  const [servers, serversLoading] = useServersList();
  const [itemTypes, itemTypesLoading] = useItemTypes();

  const [addItemMutation] = useAddItemMutation();

  const handleAddItem = (values: AddItemInput) => {
    addItemMutation({
      variables: {
        inputData: {
          name: values.name,
          itemType: values.itemType,
          server: values.server,
          price: values.price,
          requestContact: values.requestContact
        }
      }
    }).then(() => {
      setResultVisible([true, true]);
      setTimeout(() => {
        setAddItemVisible(false);
        setResultVisible([false, true]);
      }, 2000);
    }).catch(() => {
      setResultVisible([true, false]);
      setTimeout(() => {
        setAddItemVisible(false);
        setResultVisible([false, false]);
      }, 2000);
    })
  }
  const onSelect = useCallback((server: string, ratio: number) => {
    setSelectedServer(server);
    setAdenaRatio(ratio);
  }, []);

  return (
    <Layout>
      <Sider
        collapsible
        collapsed={isCollapsed}
        onCollapse={(collapsed) => { setIsCollapsed(collapsed); } }>
        {
          !isCollapsed && <SideMenu
          onSelect={onSelect}
          />
        }
      </Sider>
      <Layout>
        <Content>
          <Card style={{background: 'none'}}>
            <Row gutter={16}>
              <Col md={{span: 24, offset: 0 }} lg={{span: 14, offset: 0}} xl={{span: 14, offset: 0}}>
                <PageHeader title="Buy accounts, adena and items" />
              </Col>
              <Col md={{span: 24, offset: 0 }} lg={{span: 10, offset: 0}} xl={{span: 10, offset: 0}}>
                <AdenaCalc ratio={adenaRatio} />
              </Col>
            </Row>
            <Row gutter={16}>
              <Col md={{span: 24, offset: 0 }} lg={{span: 14, offset: 0}} xl={{span: 14, offset: 0}}>
                <div style={{display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
                  <PageHeader title="Accounts" />
                  {/* <Button>Request to add your account</Button> */}
                </div>
                <Characters serverName={selectedServer} />
              </Col>
              <Col md={{span: 24, offset: 0 }} lg={{span: 10, offset: 0}} xl={{span: 10, offset: 0}}>
                <div style={{display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
                  <PageHeader title="Items" />
                  <Button type="danger" onClick={() => { setAddItemVisible(!isAddItemVisible)} }>Request to add your item</Button>
                </div>
                <Items serverName={selectedServer} />
              </Col>
            </Row>
          </Card>
        </Content>
      </Layout>
      <Modal
        title="Add item"
        visible={isAddItemVisible}
        footer={null}
        onCancel={() => {setAddItemVisible(false)} }
      >
        {
          isResultVisible ?
          <Result status={isSuccess ? 'success' : 'error'} />
          :
          <Form {...formItemLayout} onFinish={(values) => {
            handleAddItem(values as AddItemInput)
          } }>
            <Form.Item label="Name" name="name" rules={[{required: true}]}>
              <Input />
            </Form.Item>
            <Form.Item label="Type" name="itemType" rules={[{required: true}]}>
              <Select>
                {
                  itemTypes &&
                  !itemTypesLoading &&
                  itemTypes.map(itemType => <Option value={itemType.name} key={itemType.id}>{itemType.name}</Option>)
                }
              </Select>
            </Form.Item>
            <Form.Item label="Server" name="server" rules={[{required: true}]}>
              <Select>
              {
                servers &&
                !serversLoading &&
                servers.map(server => <Option value={server.name} key={server.id}>{server.name}</Option>)
              }
              </Select>
            </Form.Item>
            <Form.Item label="Contact" name="requestContact" rules={[{required: true}]}>
              <Input placeholder="Place here link to your contact (mail, discord etc)" />
            </Form.Item>
            <Form.Item label="Price" name="price" rules={[{required: true}]}>
              <InputNumber formatter={value => `$ ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')} style={{width: '100%'}} />
            </Form.Item>
            <Button htmlType="submit" style={{width: '100%'}}>Submit</Button>
          </Form>
        }
      </Modal>
      <Modal
        title="Add character"
        visible={isAddCharacterVisible}
        onCancel={() => {setCharacterVisible(false)} }
      >
        modal content
      </Modal>
    </Layout>
  );
};

export {Shop};

const formItemLayout = {
  labelCol: {
    span: 4
  },
  wrapperCol: {
    span: 20
  },
};
