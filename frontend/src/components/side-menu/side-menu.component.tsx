import * as React from "react";
import {useEffect, useState} from 'react';
import { useServersList } from "services/characters-to-table/characters-to-table";

import { Menu, Typography } from "antd";

const { Title } = Typography;

interface IProps {
  onSelect?: (item: string, ratio: number) => void;
}

const SideMenu: React.FC<IProps> = ({ onSelect }) => {

  const [servers, serversLoading] = useServersList();
  const [selected, setSelected] = useState<string>('');

  const handleSelect = (evt: any) => {
    if (evt.key === 'all') {
      onSelect && onSelect("", 0);
      setSelected('all');
      return;
    }
    const selectedServer = servers.find(server => server.id === evt.key);
    selectedServer && onSelect && onSelect(selectedServer.name, selectedServer.adenaRatio);
    selectedServer && setSelected(selectedServer.id);
  };

  useEffect(() => {
    if (servers && !serversLoading && servers.length > 0) {
      setSelected(servers[0].id);
      onSelect && onSelect(servers[0].name, servers[0].adenaRatio);
    }
  }, [servers, serversLoading, onSelect])

  return (
    <>
      <Title level={4} style={{ color: "#FFFFFF", padding: "16px" }}>
        Servers
      </Title>
      <Menu theme="dark" mode="inline" onSelect={handleSelect} defaultSelectedKeys={[selected]} selectedKeys={[selected]}>
        <Menu.Item key={"all"}>
          <span>Show all</span>
        </Menu.Item>
        {servers &&
          !serversLoading &&
          servers.map(server => {
            return (
              <Menu.Item key={server.id}>
                <span>{server.name}</span>
              </Menu.Item>
            );
          })}
      </Menu>
    </>
  );
};

export { SideMenu };
