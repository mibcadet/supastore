import gql from 'graphql-tag';
import * as ApolloReactCommon from '@apollo/react-common';
import * as ApolloReactHooks from '@apollo/react-hooks';
export type Maybe<T> = T | null;
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string,
  String: string,
  Boolean: boolean,
  Int: number,
  Float: number,
  Date: any,
};

export type AddCharacter = {
   __typename?: 'AddCharacter',
  addedCharacter?: Maybe<Character>,
};

export type AddCharacterInput = {
  name: Scalars['String'],
  heroClass: Scalars['String'],
  server: Scalars['String'],
  level: Scalars['Int'],
  description?: Maybe<Scalars['String']>,
  price: Scalars['Float'],
  requestContact: Scalars['String'],
};

export type AddItem = {
   __typename?: 'AddItem',
  addedItem?: Maybe<Item>,
};

export type AddItemInput = {
  name: Scalars['String'],
  itemType: Scalars['String'],
  server: Scalars['String'],
  price: Scalars['Float'],
  requestContact: Scalars['String'],
};

export type Character = {
   __typename?: 'Character',
  id: Scalars['ID'],
  name: Scalars['String'],
  heroClass: CharacterClass,
  server: Server,
  level: Scalars['Int'],
  description?: Maybe<Scalars['String']>,
  added: Scalars['Date'],
  price: Scalars['Float'],
  contact?: Maybe<Contact>,
  requestContact: Scalars['String'],
  status: CharacterStatus,
};

export type CharacterClass = {
   __typename?: 'CharacterClass',
  id: Scalars['ID'],
  initials: Scalars['String'],
  name: Scalars['String'],
  characterSet: Array<Character>,
};

/** An enumeration. */
export enum CharacterStatus {
  /** Approved */
  A = 'A',
  /** Waiting */
  W = 'W',
  /** Declined */
  D = 'D'
}

export type Contact = {
   __typename?: 'Contact',
  id: Scalars['ID'],
  name: Scalars['String'],
  skype?: Maybe<Scalars['String']>,
  discord?: Maybe<Scalars['String']>,
  telegram?: Maybe<Scalars['String']>,
  mail?: Maybe<Scalars['String']>,
  other?: Maybe<Scalars['String']>,
  characterSet: Array<Character>,
  itemSet: Array<Item>,
};


export type Faq = {
   __typename?: 'Faq',
  id: Scalars['ID'],
  position: Scalars['Int'],
  question: Scalars['String'],
  answer: Scalars['String'],
};

export type Item = {
   __typename?: 'Item',
  id: Scalars['ID'],
  name: Scalars['String'],
  type: ItemType,
  server: Server,
  price: Scalars['Float'],
  contact?: Maybe<Contact>,
  requestContact: Scalars['String'],
  status: ItemStatus,
};

/** An enumeration. */
export enum ItemStatus {
  /** Approved */
  A = 'A',
  /** Waiting */
  W = 'W',
  /** Declined */
  D = 'D'
}

export type ItemType = {
   __typename?: 'ItemType',
  id: Scalars['ID'],
  name: Scalars['String'],
  itemSet: Array<Item>,
};

export type Mutations = {
   __typename?: 'Mutations',
  addItem?: Maybe<AddItem>,
  addCharacter?: Maybe<AddCharacter>,
};


export type MutationsAddItemArgs = {
  inputData: AddItemInput
};


export type MutationsAddCharacterArgs = {
  inputData: AddCharacterInput
};

export type Queries = {
   __typename?: 'Queries',
  allCharacters?: Maybe<Array<Maybe<Character>>>,
  allCharacterClasses?: Maybe<Array<Maybe<CharacterClass>>>,
  allServers?: Maybe<Array<Maybe<Server>>>,
  allItems?: Maybe<Array<Maybe<Item>>>,
  allItemTypes?: Maybe<Array<Maybe<ItemType>>>,
  allFaqs?: Maybe<Array<Maybe<Faq>>>,
};

export type Server = {
   __typename?: 'Server',
  id: Scalars['ID'],
  name: Scalars['String'],
  adenaRatio: Scalars['Float'],
  characterSet: Array<Character>,
  itemSet: Array<Item>,
};

export type CharactersQueryVariables = {};


export type CharactersQuery = (
  { __typename?: 'Queries' }
  & { allCharacters: Maybe<Array<Maybe<(
    { __typename?: 'Character' }
    & Pick<Character, 'id' | 'name' | 'level' | 'description' | 'price'>
    & { heroClass: (
      { __typename?: 'CharacterClass' }
      & Pick<CharacterClass, 'id' | 'name'>
    ), server: (
      { __typename?: 'Server' }
      & Pick<Server, 'id' | 'name'>
    ), contact: Maybe<(
      { __typename?: 'Contact' }
      & Pick<Contact, 'name' | 'skype' | 'discord' | 'telegram' | 'mail' | 'other'>
    )> }
  )>>> }
);

export type FaqsQueryVariables = {};


export type FaqsQuery = (
  { __typename?: 'Queries' }
  & { allFaqs: Maybe<Array<Maybe<(
    { __typename?: 'Faq' }
    & Pick<Faq, 'id' | 'position' | 'question' | 'answer'>
  )>>> }
);

export type CharacterClassesQueryVariables = {};


export type CharacterClassesQuery = (
  { __typename?: 'Queries' }
  & { allCharacterClasses: Maybe<Array<Maybe<(
    { __typename?: 'CharacterClass' }
    & Pick<CharacterClass, 'id' | 'name'>
  )>>> }
);

export type ItemTypesQueryVariables = {};


export type ItemTypesQuery = (
  { __typename?: 'Queries' }
  & { allItemTypes: Maybe<Array<Maybe<(
    { __typename?: 'ItemType' }
    & Pick<ItemType, 'id' | 'name'>
  )>>> }
);

export type ItemsQueryVariables = {};


export type ItemsQuery = (
  { __typename?: 'Queries' }
  & { allItems: Maybe<Array<Maybe<(
    { __typename?: 'Item' }
    & Pick<Item, 'id' | 'name' | 'price' | 'status'>
    & { server: (
      { __typename?: 'Server' }
      & Pick<Server, 'name'>
    ), type: (
      { __typename?: 'ItemType' }
      & Pick<ItemType, 'id' | 'name'>
    ), contact: Maybe<(
      { __typename?: 'Contact' }
      & Pick<Contact, 'skype' | 'discord' | 'telegram' | 'mail' | 'other'>
    )> }
  )>>> }
);

export type AddItemMutationVariables = {
  inputData: AddItemInput
};


export type AddItemMutation = (
  { __typename?: 'Mutations' }
  & { addItem: Maybe<(
    { __typename?: 'AddItem' }
    & { addedItem: Maybe<(
      { __typename?: 'Item' }
      & Pick<Item, 'name' | 'price'>
      & { server: (
        { __typename?: 'Server' }
        & Pick<Server, 'name'>
      ), type: (
        { __typename?: 'ItemType' }
        & Pick<ItemType, 'name'>
      ), contact: Maybe<(
        { __typename?: 'Contact' }
        & Pick<Contact, 'skype' | 'discord' | 'telegram' | 'mail' | 'other'>
      )> }
    )> }
  )> }
);

export type ServersQueryVariables = {};


export type ServersQuery = (
  { __typename?: 'Queries' }
  & { allServers: Maybe<Array<Maybe<(
    { __typename?: 'Server' }
    & Pick<Server, 'id' | 'name' | 'adenaRatio'>
  )>>> }
);


export const CharactersDocument = gql`
    query characters {
  allCharacters {
    id
    name
    heroClass {
      id
      name
    }
    level
    description
    server {
      id
      name
    }
    price
    contact {
      name
      skype
      discord
      telegram
      mail
      other
    }
  }
}
    `;

/**
 * __useCharactersQuery__
 *
 * To run a query within a React component, call `useCharactersQuery` and pass it any options that fit your needs.
 * When your component renders, `useCharactersQuery` returns an object from Apollo Client that contains loading, error, and data properties 
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useCharactersQuery({
 *   variables: {
 *   },
 * });
 */
export function useCharactersQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<CharactersQuery, CharactersQueryVariables>) {
        return ApolloReactHooks.useQuery<CharactersQuery, CharactersQueryVariables>(CharactersDocument, baseOptions);
      }
export function useCharactersLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<CharactersQuery, CharactersQueryVariables>) {
          return ApolloReactHooks.useLazyQuery<CharactersQuery, CharactersQueryVariables>(CharactersDocument, baseOptions);
        }
export type CharactersQueryHookResult = ReturnType<typeof useCharactersQuery>;
export type CharactersLazyQueryHookResult = ReturnType<typeof useCharactersLazyQuery>;
export type CharactersQueryResult = ApolloReactCommon.QueryResult<CharactersQuery, CharactersQueryVariables>;
export const FaqsDocument = gql`
    query faqs {
  allFaqs {
    id
    position
    question
    answer
  }
}
    `;

/**
 * __useFaqsQuery__
 *
 * To run a query within a React component, call `useFaqsQuery` and pass it any options that fit your needs.
 * When your component renders, `useFaqsQuery` returns an object from Apollo Client that contains loading, error, and data properties 
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useFaqsQuery({
 *   variables: {
 *   },
 * });
 */
export function useFaqsQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<FaqsQuery, FaqsQueryVariables>) {
        return ApolloReactHooks.useQuery<FaqsQuery, FaqsQueryVariables>(FaqsDocument, baseOptions);
      }
export function useFaqsLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<FaqsQuery, FaqsQueryVariables>) {
          return ApolloReactHooks.useLazyQuery<FaqsQuery, FaqsQueryVariables>(FaqsDocument, baseOptions);
        }
export type FaqsQueryHookResult = ReturnType<typeof useFaqsQuery>;
export type FaqsLazyQueryHookResult = ReturnType<typeof useFaqsLazyQuery>;
export type FaqsQueryResult = ApolloReactCommon.QueryResult<FaqsQuery, FaqsQueryVariables>;
export const CharacterClassesDocument = gql`
    query characterClasses {
  allCharacterClasses {
    id
    name
  }
}
    `;

/**
 * __useCharacterClassesQuery__
 *
 * To run a query within a React component, call `useCharacterClassesQuery` and pass it any options that fit your needs.
 * When your component renders, `useCharacterClassesQuery` returns an object from Apollo Client that contains loading, error, and data properties 
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useCharacterClassesQuery({
 *   variables: {
 *   },
 * });
 */
export function useCharacterClassesQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<CharacterClassesQuery, CharacterClassesQueryVariables>) {
        return ApolloReactHooks.useQuery<CharacterClassesQuery, CharacterClassesQueryVariables>(CharacterClassesDocument, baseOptions);
      }
export function useCharacterClassesLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<CharacterClassesQuery, CharacterClassesQueryVariables>) {
          return ApolloReactHooks.useLazyQuery<CharacterClassesQuery, CharacterClassesQueryVariables>(CharacterClassesDocument, baseOptions);
        }
export type CharacterClassesQueryHookResult = ReturnType<typeof useCharacterClassesQuery>;
export type CharacterClassesLazyQueryHookResult = ReturnType<typeof useCharacterClassesLazyQuery>;
export type CharacterClassesQueryResult = ApolloReactCommon.QueryResult<CharacterClassesQuery, CharacterClassesQueryVariables>;
export const ItemTypesDocument = gql`
    query itemTypes {
  allItemTypes {
    id
    name
  }
}
    `;

/**
 * __useItemTypesQuery__
 *
 * To run a query within a React component, call `useItemTypesQuery` and pass it any options that fit your needs.
 * When your component renders, `useItemTypesQuery` returns an object from Apollo Client that contains loading, error, and data properties 
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useItemTypesQuery({
 *   variables: {
 *   },
 * });
 */
export function useItemTypesQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<ItemTypesQuery, ItemTypesQueryVariables>) {
        return ApolloReactHooks.useQuery<ItemTypesQuery, ItemTypesQueryVariables>(ItemTypesDocument, baseOptions);
      }
export function useItemTypesLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<ItemTypesQuery, ItemTypesQueryVariables>) {
          return ApolloReactHooks.useLazyQuery<ItemTypesQuery, ItemTypesQueryVariables>(ItemTypesDocument, baseOptions);
        }
export type ItemTypesQueryHookResult = ReturnType<typeof useItemTypesQuery>;
export type ItemTypesLazyQueryHookResult = ReturnType<typeof useItemTypesLazyQuery>;
export type ItemTypesQueryResult = ApolloReactCommon.QueryResult<ItemTypesQuery, ItemTypesQueryVariables>;
export const ItemsDocument = gql`
    query items {
  allItems {
    id
    name
    server {
      name
    }
    type {
      id
      name
    }
    price
    contact {
      skype
      discord
      telegram
      mail
      other
    }
    status
  }
}
    `;

/**
 * __useItemsQuery__
 *
 * To run a query within a React component, call `useItemsQuery` and pass it any options that fit your needs.
 * When your component renders, `useItemsQuery` returns an object from Apollo Client that contains loading, error, and data properties 
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useItemsQuery({
 *   variables: {
 *   },
 * });
 */
export function useItemsQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<ItemsQuery, ItemsQueryVariables>) {
        return ApolloReactHooks.useQuery<ItemsQuery, ItemsQueryVariables>(ItemsDocument, baseOptions);
      }
export function useItemsLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<ItemsQuery, ItemsQueryVariables>) {
          return ApolloReactHooks.useLazyQuery<ItemsQuery, ItemsQueryVariables>(ItemsDocument, baseOptions);
        }
export type ItemsQueryHookResult = ReturnType<typeof useItemsQuery>;
export type ItemsLazyQueryHookResult = ReturnType<typeof useItemsLazyQuery>;
export type ItemsQueryResult = ApolloReactCommon.QueryResult<ItemsQuery, ItemsQueryVariables>;
export const AddItemDocument = gql`
    mutation addItem($inputData: AddItemInput!) {
  addItem(inputData: $inputData) {
    addedItem {
      name
      server {
        name
      }
      type {
        name
      }
      price
      contact {
        skype
        discord
        telegram
        mail
        other
      }
    }
  }
}
    `;
export type AddItemMutationFn = ApolloReactCommon.MutationFunction<AddItemMutation, AddItemMutationVariables>;

/**
 * __useAddItemMutation__
 *
 * To run a mutation, you first call `useAddItemMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useAddItemMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [addItemMutation, { data, loading, error }] = useAddItemMutation({
 *   variables: {
 *      inputData: // value for 'inputData'
 *   },
 * });
 */
export function useAddItemMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<AddItemMutation, AddItemMutationVariables>) {
        return ApolloReactHooks.useMutation<AddItemMutation, AddItemMutationVariables>(AddItemDocument, baseOptions);
      }
export type AddItemMutationHookResult = ReturnType<typeof useAddItemMutation>;
export type AddItemMutationResult = ApolloReactCommon.MutationResult<AddItemMutation>;
export type AddItemMutationOptions = ApolloReactCommon.BaseMutationOptions<AddItemMutation, AddItemMutationVariables>;
export const ServersDocument = gql`
    query servers {
  allServers {
    id
    name
    adenaRatio
  }
}
    `;

/**
 * __useServersQuery__
 *
 * To run a query within a React component, call `useServersQuery` and pass it any options that fit your needs.
 * When your component renders, `useServersQuery` returns an object from Apollo Client that contains loading, error, and data properties 
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useServersQuery({
 *   variables: {
 *   },
 * });
 */
export function useServersQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<ServersQuery, ServersQueryVariables>) {
        return ApolloReactHooks.useQuery<ServersQuery, ServersQueryVariables>(ServersDocument, baseOptions);
      }
export function useServersLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<ServersQuery, ServersQueryVariables>) {
          return ApolloReactHooks.useLazyQuery<ServersQuery, ServersQueryVariables>(ServersDocument, baseOptions);
        }
export type ServersQueryHookResult = ReturnType<typeof useServersQuery>;
export type ServersLazyQueryHookResult = ReturnType<typeof useServersLazyQuery>;
export type ServersQueryResult = ApolloReactCommon.QueryResult<ServersQuery, ServersQueryVariables>;