import { useEffect, useState } from "react";
import {
  useCharactersQuery,
  Character,
  useCharacterClassesQuery,
  CharacterClass,
  useServersQuery,
  Server,
} from "lib/graphql";

export interface ICharacter {
  key: string;
  name: string;
  heroClass: string;
  level: number;
  gameServer: string;
  description: string;
  price: number;
}

export const useCharactersList = (): [ICharacter[], any] => {
  const { data, loading, error } = useCharactersQuery();
  const [characters, setCharacters] = useState<ICharacter[]>([]);

  useEffect(() => {
    if (!loading && !error && data) {
      const allCharacters = (data.allCharacters as Character[]) || [];

      const result: ICharacter[] = allCharacters.map((character: Character) => {
        return {
          key: character.id,
          name: character.name,
          heroClass: character.heroClass.name,
          level: character.level,
          gameServer: character.server.name,
          description: character.description ? character.description : '',
          price: character.price,
          contact: character.contact
        };
      });

      setCharacters(result);
    }
  }, [data, loading, error]);

  return [characters, loading];
};

export interface ICharacterClass {
  id: string;
  name: string;
}

export const useCharacterClassesList = (): [ICharacterClass[], any] => {
  const { data, loading, error } = useCharacterClassesQuery();
  const [characterClasses, setCharacterClasses] = useState<ICharacterClass[]>(
    []
  );

  useEffect(() => {
    if (!loading && !error && data) {
      const allCharacterClasses = (data.allCharacterClasses as CharacterClass[]) || [];
      setCharacterClasses(
        allCharacterClasses.map(elem => ({
          id: elem.id,
          name: elem.name
        }))
      );
    }
  }, [data, loading, error]);

  return [characterClasses, loading];
};

export interface IServer {
  id: string;
  name: string;
  adenaRatio: number;
}

export const useServersList = (): [IServer[], boolean] => {
  const { data, loading, error } = useServersQuery();
  const [gameservers, setServer] = useState<IServer[]>([]);

  useEffect(() => {
    if (!loading && !error && data) {
      const allServers = (data.allServers as Server[]) || [];
      setServer(
        allServers.map(elem => ({
          id: elem.id,
          name: elem.name,
          adenaRatio: elem.adenaRatio,
        }))
      );
    }
  }, [data, loading, error]);

  return [gameservers, loading];
};
