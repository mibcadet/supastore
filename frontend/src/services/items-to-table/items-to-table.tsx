import { useEffect, useState } from "react";
import { useItemsQuery, Item, ItemType, useItemTypesQuery } from "lib/graphql";

export interface IItem {
  key: string;
  name: string;
  server: string;
  type: string;
  price: number;
}

export const useItemsList = (): [IItem[], boolean] => {
  const { data, loading, error } = useItemsQuery();
  const [items, setItems] = useState<IItem[]>([]);

  useEffect(() => {
    if (!loading && !error && data) {
      const allItems = (data.allItems as Item[]) || [];

      const result: IItem[] = allItems.map((item: Item) => {
        return {
          key: item.id,
          name: item.name,
          server: item.server.name,
          type: item.type.name,
          price: item.price,
          contact: item.contact
        };
      });

      setItems(result);
    }
  }, [data, loading, error]);

  return [items, loading];
};

export interface IItemType {
  id: string;
  name: string;
}

export const useItemTypes = (): [IItemType[], boolean] => {
  const { data, loading, error } = useItemTypesQuery();
  const [types, setTypes] = useState<IItemType[]>([]);

  useEffect(() => {
    if (!loading && !error && data) {
      const allItemTypes = (data.allItemTypes as ItemType[]) || [];
      setTypes(
        allItemTypes.map(elem => ({
          id: elem.id,
          name: elem.name
        }))
      );
    }
  }, [data, loading, error]);

  return [types, loading];
};
