import { ReactWrapper } from 'enzyme';
import { act } from 'react-dom/test-utils';

export const waitForComponentToPaint = async function waitForComponentToPaint<P = {}>(
  wrapper: ReactWrapper<P>,
  amount = 0,
) {
  //@ts-ignore fix enzyme update issue
  await act(async () => {
    await new Promise(resolve => setTimeout(resolve, amount));
    wrapper.update();
  });
}
