#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'

APP_DIR=$(dirname "$(dirname "$0")")
cd "${APP_DIR}"

APPLICATION_NAME=shop
APPLICATION_PATH=/opt/$APPLICATION_NAME
PRODUCTION_BACKEND_PATH=$APPLICATION_PATH/backend
PRODUCTION_FRONTEND_PATH=$APPLICATION_PATH/frontend/

PRODUCTION_SETTINGS_DESTINATION=$PRODUCTION_BACKEND_PATH/dserver/dserver/production_settings.py

LOCAL_FRONTEND_BUNDLE_PATH=$1
LOCAL_BACKEND_PATH=$2
LOCAL_DEPLOYMENT_CONFIGURATION=$3

export DJANGO_SETTINGS_MODULE=dserver.production_settings

function try_migration() {
    cp -r $LOCAL_DEPLOYMENT_CONFIGURATION/production_settings.py ./backend/dserver/dserver

    echo 'Instaling dependencies..'
    ./backend/scripts/setup.sh --system

    echo 'Migrating..'
    if [ -f "$PRODUCTION_BACKEND_PATH/dserver/db.sqlite3" ]; then
        cp $PRODUCTION_BACKEND_PATH/dserver/db.sqlite3 ./backend/dserver/db.sqlite3
    else
        rm ./backend/dserver/db.sqlite3
    fi

    ./backend/scripts/migrate.sh
    echo "Migration result: $?"
    if [ $? -ne 0 ]; then
        echo 'Could not migrate.. Do it manually!'
        exit;
    fi
}

function copy_backend() {
    echo 'Copy backend files..'
    mkdir -p $PRODUCTION_BACKEND_PATH
    systemctl stop gunicorn
    systemctl stop nginx

    cp -r $LOCAL_BACKEND_PATH $PRODUCTION_BACKEND_PATH

    cp -r $LOCAL_DEPLOYMENT_CONFIGURATION/production_settings.py $PRODUCTION_SETTINGS_DESTINATION
    cp -r $LOCAL_DEPLOYMENT_CONFIGURATION/default /etc/nginx/sites-enabled/default
    cp -r $LOCAL_DEPLOYMENT_CONFIGURATION/nginx.conf /etc/nginx/nginx.conf
    cp -r $LOCAL_DEPLOYMENT_CONFIGURATION/gunicorn.service /etc/systemd/system/gunicorn.service
    cp -r $LOCAL_DEPLOYMENT_CONFIGURATION/db_backup.timer /etc/systemd/system/db_backup.timer
    cp -r $LOCAL_DEPLOYMENT_CONFIGURATION/db_backup.service /etc/systemd/system/db_backup.service
    cp -r $LOCAL_DEPLOYMENT_CONFIGURATION/db_backup.sh $APPLICATION_PATH/db_backup.sh

    python3 $PRODUCTION_BACKEND_PATH/dserver/manage.py collectstatic --no-input

    systemctl daemon-reload
    systemctl start gunicorn
    systemctl start nginx
    systemctl enable db_backup.timer
    systemctl start db_backup.timer
    systemctl start db_backup
}

function copy_frontend() {
    echo 'Copy frontend files..'
    mkdir -p $PRODUCTION_FRONTEND_PATH
    cp -r $LOCAL_FRONTEND_BUNDLE_PATH $PRODUCTION_FRONTEND_PATH
}

echo 'Deploying..'
try_migration
copy_backend
copy_frontend
