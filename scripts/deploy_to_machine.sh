#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'

APP_DIR=$(dirname "$(dirname "$0")")
cd "${APP_DIR}"

APPLICATION_NAME=shop

DEPLOY_USERNAME=root
DEPLOY_HOST=51.15.77.135

NOW_TIME=`date '+%d_%m_%Y-%H_%M_%S'`
TMP_PATH=/tmp/$APPLICATION_NAME/$NOW_TIME

LOCAL_FRONTEND_BUILD_PATH=$1
LOCAL_BACKEND_PATH=$2
LOCAL_DEPLOYMENT_CONFIGURATION=$3
LOCAL_SCRIPTS_PATH=scripts

function copy_files_to_machine() {

    URI=$DEPLOY_USERNAME@$DEPLOY_HOST
    echo "Deploying to $URI"
    ssh $URI "mkdir -p $TMP_PATH" 
    rsync -avzP -e ssh $LOCAL_BACKEND_PATH $URI:$TMP_PATH
    rsync -avzP -e ssh $LOCAL_FRONTEND_BUILD_PATH $URI:$TMP_PATH
    rsync -avzP -e ssh $LOCAL_DEPLOYMENT_CONFIGURATION $URI:$TMP_PATH
    rsync -avzP -e ssh $LOCAL_SCRIPTS_PATH $URI:$TMP_PATH
}

copy_files_to_machine
TMP_DEPLOY_PATH=$TMP_PATH/scripts/deploy.sh
ssh $URI "chmod +x $TMP_DEPLOY_PATH && $TMP_DEPLOY_PATH ./build/* ./backend/* ./deployment/"