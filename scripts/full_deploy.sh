#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'

APP_DIR=$(dirname "$(dirname "$0")")
cd "${APP_DIR}"

yarn --cwd frontend build
./scripts/deploy_to_machine.sh frontend/build backend deployment